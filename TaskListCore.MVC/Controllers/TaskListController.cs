using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TaskListCore.Domain;

namespace TaskListCore.MVC.Controllers
{
    public class TaskListController : Controller
    {
        private ITaskListService _Service;

        public TaskListController(ITaskListService service)
        {
            _Service = service;
        }

        // GET: TaskList
        public ActionResult Index()
        {
            var tasklist = _Service.GetTaskListItems();
            return View(tasklist);
        }

        // GET: TaskList/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: TaskList/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(TaskListItem model)
        {
            if (ModelState.IsValid)
            {
                _Service.AddNewTask(model);
                return RedirectToAction("Index");
            }              
            else
            {
                return View();
            }
        }

        // GET: TaskList/Complete/5
        public ActionResult Complete(int id)
        {
            var task = _Service.GetTask(id);
            if (task.Execute())
            {
                _Service.UpdateTask(task);
            }
            return RedirectToAction("Index");
        }

        // GET: TaskList/Delete/5
        public ActionResult Delete(int id)
        {
            var task = _Service.GetTask(id);
            _Service.DeleteTask(task);
            return RedirectToAction("Index");
        } 

        public ActionResult Error()
        {
            return View();
        }

    }
}