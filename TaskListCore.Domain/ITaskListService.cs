﻿using System.Linq;

namespace TaskListCore.Domain
{
    public interface ITaskListService
    {
        void AddNewTask(TaskListItem task);
        void DeleteTask(TaskListItem task);
        TaskListItem GetTask(int id);
        IQueryable<TaskListItem> GetTaskListItems();
        void UpdateTask(TaskListItem task);
    }
}