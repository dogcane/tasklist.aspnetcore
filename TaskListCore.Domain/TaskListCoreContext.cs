﻿using Microsoft.EntityFrameworkCore;

namespace TaskListCore.Domain
{
    public class TaskListCoreContext : DbContext
    {
        public DbSet<TaskListItem> TaskListItems { get; set; }

        public TaskListCoreContext(DbContextOptions<TaskListCoreContext> options)
            : base(options)
        { }
    }
}
