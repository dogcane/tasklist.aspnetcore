﻿using System.Linq;

namespace TaskListCore.Domain
{
    public class TaskListService : ITaskListService
    {
        private TaskListCoreContext _Context;

        public TaskListService(TaskListCoreContext context)
        {
            _Context = context;
        }

        public void AddNewTask(TaskListItem task)
        {
            _Context.Add(task);
            _Context.SaveChanges();
        }

        public void UpdateTask(TaskListItem task)
        {
            _Context.Update(task);
            _Context.SaveChanges();
        }

        public void DeleteTask(TaskListItem task)
        {
            _Context.Remove(task);
            _Context.SaveChanges();
        }

        public TaskListItem GetTask(int id)
        {
            return _Context.TaskListItems.Find(id);
        }

        public IQueryable<TaskListItem> GetTaskListItems()
        {
            return _Context.TaskListItems;
        }
    }    
}
