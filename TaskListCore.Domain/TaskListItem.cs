﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TaskListCore.Domain
{
    public class TaskListItem
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [MaxLength(100, ErrorMessage = "Task not valid")]
        public string Task { get; set; }

        public DateTime? ExecutedOn { get; set; }

        public bool Executed { get; set; }

        public bool Execute()
        {
            if (!Executed)
            {
                Executed = true;
                ExecutedOn = DateTime.Now;
                return true;
            }
            return false;
        }
    }
}
